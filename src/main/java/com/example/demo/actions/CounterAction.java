package com.example.demo.actions;

import com.example.demo.Task;
import com.example.demo.feign.GenreClient;
import com.example.demo.feign.GenresWrapper;
import com.example.demo.feign.MovieClient;
import com.example.demo.feign.MoviesWrapper;
import com.example.demo.models.Genre;
import com.example.demo.models.Movie;
import com.example.demo.properties.MovieServiceProperties;
import com.example.demo.services.ResultService;
import com.example.demo.services.TaskService;
import com.example.demo.services.UniqueMovieChecker;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.function.Predicate;

import static com.example.demo.TaskStatus.DONE;
import static java.lang.String.format;
import static java.lang.Thread.currentThread;

@Slf4j
@RequiredArgsConstructor
public final class CounterAction implements Runnable {
    private final MovieClient movieClient;
    private final GenreClient genreClient;
    private final MovieServiceProperties serviceProperties;
    private final Predicate<Movie> filter;
    private final ResultService resultService;
    private final TaskService taskService;
    private final Task task;

    private boolean inProgress = true;

    @Override
    public void run() {
        currentThread().setName("counter for " + task.getId());

        validateGenre(task);

        taskService.changeStatusToProcessing(task);

        UniqueMovieChecker checker = new UniqueMovieChecker();

        long page = 0;

        while (inProgress) {
            if (taskService.isCanceled(task)) {
                stopProgress();
                break;
            }

            if (currentThread().isInterrupted()) {
                stopProgress();
                log.error("Thread interrupted");
                taskService.completeWithError(task, "thread interrupted");
            } else {
                page++;
                try {
                    log.info("Trying to load movies from page {}", page);
                    MoviesWrapper loaded = movieClient.getFromPage(serviceProperties.getApiKey(), page);

                    log.info("Loaded {} movies from page {}", loaded.getResults().size(), page);
                    processMovies(filter, checker, loaded);
                } catch (Exception e) {
                    log.warn("Unable to load movies from page {}", page, e);

                    stopProgress();

                    if (e instanceof FeignException) {
                        taskService.complete(task);
                    } else {
                        taskService.completeWithError(task, e.toString());
                    }
                }
            }
        }
    }

    private void validateGenre(Task task) {
        try {
            GenresWrapper genres = genreClient.getGenres(serviceProperties.getApiKey());

            Optional<Genre> found = genres.getGenres().stream()
                    .filter(g -> g.getId() == task.getGenre().getId())
                    .findAny();

            if (!found.isPresent()) {
                String msg = format("Unable to complete a task %s - specified genre doesn't exists", task);
                log.error(msg);
                taskService.completeWithError(task, msg);
            } else {
                taskService.update(task, t -> {
                    Genre genre = new Genre(t.getGenre().getId(), found.get().getName());
                    return new Task(t.getId(), genre, t.getStatus(), t.getNote());
                });
            }
        } catch (FeignException e) {
            log.error("Unable to fet genres", e);
            taskService.completeWithError(task, "unable to fetch genres " + e.getMessage());
        }
    }

    private void stopProgress() {
        inProgress = false;
    }

    private void processMovies(Predicate<Movie> filter, UniqueMovieChecker checker, MoviesWrapper loaded) {
        for (Movie movie : loaded.getResults()) {
            if (movie != null && movie.getVote_average() != null && filter.test(movie)
                    && checker.rememberIfUnique(movie)) {
                resultService.addToRate(task, movie.getVote_average());
            }
        }
    }
}
