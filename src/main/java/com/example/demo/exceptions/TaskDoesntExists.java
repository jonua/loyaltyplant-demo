package com.example.demo.exceptions;

import java.util.UUID;

public class TaskDoesntExists extends RuntimeException {
    public TaskDoesntExists(UUID id) {
        super("Task " + id + " doesn't exists");
    }
}
