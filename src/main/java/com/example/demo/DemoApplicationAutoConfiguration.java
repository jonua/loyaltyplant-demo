package com.example.demo;

import com.example.demo.feign.ClientFactory;
import com.example.demo.feign.GenreClient;
import com.example.demo.feign.MovieClient;
import com.example.demo.properties.MovieServiceProperties;
import com.example.demo.services.ArithmeticalMeanAverageCounter;
import com.example.demo.services.AverageCounter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@ComponentScan
@RequiredArgsConstructor
@EnableConfigurationProperties({MovieServiceProperties.class})
public class DemoApplicationAutoConfiguration {
    private final MovieServiceProperties properties;

    @Bean
    public ClientFactory movieClientFactory() {
        return new ClientFactory(properties);
    }

    @Bean
    public MovieClient movieClient(ClientFactory factory) {
        return factory.createSimpleMovieClient();
    }

    @Bean
    public GenreClient genreClient(ClientFactory factory) {
        return factory.createSimpleGenreClient();
    }

    @Bean
    @Qualifier("counterExecutorService")
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(10);
    }

    @Bean
    @ConditionalOnMissingBean
    public AverageCounter averageCounter() {
        return new ArithmeticalMeanAverageCounter();
    }
}
