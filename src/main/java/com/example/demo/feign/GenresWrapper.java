package com.example.demo.feign;

import com.example.demo.models.Genre;
import lombok.Data;

import java.util.List;

@Data
public class GenresWrapper {
    private List<Genre> genres;
}
