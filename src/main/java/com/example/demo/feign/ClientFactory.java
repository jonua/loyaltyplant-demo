package com.example.demo.feign;

import com.example.demo.properties.MovieServiceProperties;
import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class ClientFactory {
    private final MovieServiceProperties properties;

    public MovieClient createSimpleMovieClient() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(MovieClient.class))
                .logLevel(Logger.Level.BASIC)
                .target(MovieClient.class, properties.getHost());
    }

    public GenreClient createSimpleGenreClient() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(GenreClient.class))
                .logLevel(Logger.Level.BASIC)
                .target(GenreClient.class, properties.getHost());
    }
}
