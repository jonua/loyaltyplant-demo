package com.example.demo.feign;

import feign.Param;
import feign.RequestLine;

public interface MovieClient {
    @RequestLine("GET /3/discover/movie?api_key={apiKey}&page={page}")
    MoviesWrapper getFromPage(@Param("apiKey") String apiKey, @Param("page") long page);
}
