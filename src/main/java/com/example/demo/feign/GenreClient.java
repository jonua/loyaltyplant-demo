package com.example.demo.feign;

import feign.Param;
import feign.RequestLine;

public interface GenreClient {
    @RequestLine("GET /3/genre/movie/list?api_key={apiKey}")
    GenresWrapper getGenres(@Param("apiKey") String apiKey);
}
