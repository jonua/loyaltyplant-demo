package com.example.demo.feign;

import com.example.demo.models.Movie;
import lombok.Data;

import java.util.Set;

@Data
public class MoviesWrapper {
    private Set<Movie> results;
}
