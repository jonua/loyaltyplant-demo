package com.example.demo;

public enum TaskStatus {
    NEW,
    PROCESSING,
    DONE,
    CANCELED,
    ERROR,
}
