package com.example.demo.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "third-party.movie")
public class MovieServiceProperties {
    private String host;
    private String apiKey;
}
