package com.example.demo.services;

import com.example.demo.Task;
import com.example.demo.actions.CounterAction;
import com.example.demo.feign.GenreClient;
import com.example.demo.feign.MovieClient;
import com.example.demo.models.Genre;
import com.example.demo.models.Movie;
import com.example.demo.properties.MovieServiceProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.function.Predicate;

@Slf4j
@Service
@RequiredArgsConstructor
public final class SimpleCounterService implements CounterService {
    private final MovieClient movieClient;
    private final GenreClient genreClient;
    private final ResultService resultService;
    private final TaskService taskService;
    private final MovieServiceProperties props;
    @Qualifier("counterExecutorService")
    private final ExecutorService counterExecutorService;

    @Override
    public Task startCounting(Genre genre) {
        Task task = taskService.createTask(genre);

        log.info("Created new task {}", task);
        CounterAction counterAction = new CounterAction(movieClient, genreClient, props, movieFilter(genre),
                resultService, taskService, task);

        counterExecutorService.execute(counterAction);

        return task;
    }

    private Predicate<Movie> movieFilter(Genre genre) {
        return m -> m.getGenre_ids().contains(genre.getId());
    }
}
