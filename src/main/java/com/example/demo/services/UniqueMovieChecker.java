package com.example.demo.services;

import com.example.demo.models.Movie;

import java.util.HashSet;
import java.util.Set;

public final class UniqueMovieChecker {
    private final Set<Long> set = new HashSet<>();

    public boolean rememberIfUnique(Movie movie) {
        return set.add(movie.getId());
    }
}
