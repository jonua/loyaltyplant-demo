package com.example.demo.services;

import com.example.demo.Task;
import com.example.demo.models.Genre;

public interface CounterService {
    Task startCounting(Genre genre);
}
