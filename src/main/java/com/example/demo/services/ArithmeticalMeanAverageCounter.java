package com.example.demo.services;

import java.util.Collection;
import java.util.Objects;

public class ArithmeticalMeanAverageCounter implements AverageCounter {
    @Override
    public double calculate(Collection<Double> range) {
        if (range == null || range.isEmpty()) {
            return 0D;
        } else {
            Double sum = range.stream()
                    .filter(Objects::nonNull)
                    .reduce(0D, Double::sum);

            if (sum == 0) {
                return 0D;
            } else {
                return sum / range.size();
            }
        }
    }
}
