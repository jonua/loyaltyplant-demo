package com.example.demo.services;

import com.example.demo.Task;
import com.example.demo.models.Genre;
import com.example.demo.repositories.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.function.UnaryOperator;

import static com.example.demo.TaskStatus.NEW;

@Service
@RequiredArgsConstructor
public class DefaultTaskService implements TaskService {
    private final TaskRepository taskRepository;

    @Override
    public void complete(Task task) {
        taskRepository.chaneStatusToDone(task.getId());
    }

    @Override
    public void completeWithError(Task task, String message) {
        taskRepository.changeStatusToError(task.getId(), message);
    }

    @Override
    public void cancel(Task task) {
        taskRepository.changeStatusToStop(task.getId());
    }

    @Override
    public Task createTask(Genre genre) {
        Task task = new Task(UUID.randomUUID(), genre, NEW, "");
        return taskRepository.create(task);
    }

    @Override
    public Optional<Task> find(UUID taskId) {
        return taskRepository.find(taskId);
    }

    @Override
    public void changeStatusToProcessing(Task task) {
        taskRepository.changeStatusToProcessing(task.getId());
    }

    @Override
    public boolean isCanceled(Task task) {
        return taskRepository.isCancelled(task);
    }

    @Override
    public void update(Task task, UnaryOperator<Task> taskUpdater) {
        taskRepository.updateTask(task, taskUpdater);
    }
}
