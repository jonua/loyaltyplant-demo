package com.example.demo.services;

import java.util.Collection;

public interface AverageCounter {
    double calculate(Collection<Double> range);
}
