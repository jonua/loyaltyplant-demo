package com.example.demo.services;

import com.example.demo.CountingResult;
import com.example.demo.Task;
import com.example.demo.exceptions.TaskDoesntExists;
import com.example.demo.repositories.ResultRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DefaultResultService implements ResultService {
    private final ResultRepository resultRepository;
    private final TaskService taskService;
    private final AverageCounter averageCounter;

    @Override
    public void addToRate(Task task, Double voteAverage) {
        resultRepository.addToRate(task, voteAverage);
    }

    @Override
    public CountingResult extractForTask(Task task) {
        List<Double> rates = resultRepository.getRates(task);

        Double avg = calculateAverage(rates);

        Task found = taskService.find(task.getId()).orElse(null);
        if (found != null) {
            return new CountingResult(found, avg, rates.size());
        } else {
            throw new TaskDoesntExists(task.getId());
        }
    }

    private Double calculateAverage(List<Double> rates) {
        return averageCounter.calculate(rates);
    }
}
