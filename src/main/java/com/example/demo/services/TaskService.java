package com.example.demo.services;

import com.example.demo.Task;
import com.example.demo.models.Genre;

import java.util.Optional;
import java.util.UUID;
import java.util.function.UnaryOperator;

public interface TaskService {
    void complete(Task task);

    void completeWithError(Task task, String thread_interrupted);

    void cancel(Task task);

    Task createTask(Genre genre);

    Optional<Task> find(UUID taskId);

    void changeStatusToProcessing(Task task);

    boolean isCanceled(Task task);

    void update(Task task, UnaryOperator<Task> taskUpdater);
}
