package com.example.demo.services;

import com.example.demo.CountingResult;
import com.example.demo.Task;

public interface ResultService {
    void addToRate(Task task, Double voteAverage);

    CountingResult extractForTask(Task task);
}
