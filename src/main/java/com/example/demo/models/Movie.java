package com.example.demo.models;

import lombok.Data;

import java.util.Set;

@Data
public class Movie {
    private final Long id;
    private final Set<Long> genre_ids;
    private final Double vote_average;
}
