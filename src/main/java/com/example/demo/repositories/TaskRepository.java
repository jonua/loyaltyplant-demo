package com.example.demo.repositories;

import com.example.demo.Task;

import java.util.Optional;
import java.util.UUID;
import java.util.function.UnaryOperator;

public interface TaskRepository {
    Task create(Task task);

    Optional<Task> find(UUID taskId);

    void chaneStatusToDone(UUID taskId);

    void changeStatusToError(UUID taskId, String message);

    void changeStatusToStop(UUID taskId);

    void changeStatusToProcessing(UUID task);

    boolean isCancelled(Task task);

    void updateTask(Task task, UnaryOperator<Task> taskUpdater);
}
