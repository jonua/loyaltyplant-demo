package com.example.demo.repositories;

import com.example.demo.Task;
import com.example.demo.TaskStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import static com.example.demo.TaskStatus.*;
import static java.lang.String.format;
import static java.util.Optional.ofNullable;

@Slf4j
@Repository
public class InMemoryTaskRepository implements TaskRepository {
    private static final Predicate<Task> IS_CANCELED_FILTER = t -> CANCELED.equals(t.getStatus());

    private final Map<UUID, Task> tasks = new ConcurrentHashMap<>();

    @Override
    public Task create(Task task) {
        tasks.put(task.getId(), task);
        return task;
    }

    @Override
    public Optional<Task> find(UUID taskId) {
        return ofNullable(tasks.get(taskId));
    }

    @Override
    public void chaneStatusToDone(UUID taskId) {
        tasks.computeIfPresent(taskId, (id, t) -> {
            if (PROCESSING.equals(t.getStatus())) {
                log.info("Changing status of task {} from {} to {}", t.getId(), t.getStatus(), DONE);
                return new Task(t.getId(), t.getGenre(), DONE, t.getNote());
            } else {
                String msg = format("Unable to change status for task %s to %s. Task must be in %s status; current status %s",
                        t, DONE, NEW, t.getStatus());
                log.warn(msg);
            }

            return t;
        });
    }

    @Override
    public void changeStatusToError(UUID taskId, final String message) {
        List<TaskStatus> allowedStatuses = Arrays.asList(NEW, PROCESSING);

        tasks.computeIfPresent(taskId, (id, t) -> {
            if (allowedStatuses.contains(t.getStatus())) {
                log.info("Changing status of task {} from {} to {}", t.getId(), t.getStatus(), ERROR);
                return new Task(t.getId(), t.getGenre(), ERROR, message);
            } else {
                String msg = format("Unable to change status for task %s to %s. Task must be in %s status; current status %s",
                        t, ERROR, NEW, t.getStatus());
                log.warn(msg);
            }

            return t;
        });
    }

    @Override
    public void changeStatusToStop(UUID taskId) {
        tasks.computeIfPresent(taskId, (id, t) -> {
            List<TaskStatus> allowedStatuses = Arrays.asList(NEW, PROCESSING);

            if (allowedStatuses.contains(t.getStatus())) {
                log.info("Changing status of task {} from {} to {}", t.getId(), t.getStatus(), CANCELED);
                return new Task(t.getId(), t.getGenre(), CANCELED, t.getNote());
            } else {
                String msg = format("Unable to change status for task %s to %s. Task must be in %s status; current status %s",
                        t, CANCELED, allowedStatuses, t.getStatus());
                log.warn(msg);
            }

            return t;
        });
    }

    @Override
    public void changeStatusToProcessing(UUID taskId) {
        tasks.computeIfPresent(taskId, (id, t) -> {
            if (NEW.equals(t.getStatus())) {
                log.info("Changing status of task {} from {} to {}", t.getId(), t.getStatus(), PROCESSING);
                return new Task(t.getId(), t.getGenre(), PROCESSING, t.getNote());
            } else {
                String msg = format("Unable to change status for task %s to %s. Task must be in %s status; current status %s",
                        t, PROCESSING, NEW, t.getStatus());
                log.warn(msg);
            }

            return t;
        });
    }

    @Override
    public boolean isCancelled(Task task) {
        return ofNullable(tasks.get(task.getId()))
                .filter(IS_CANCELED_FILTER)
                .isPresent();
    }

    @Override
    public void updateTask(Task task, UnaryOperator<Task> taskUpdater) {
        tasks.computeIfPresent(task.getId(), (_it, t) -> taskUpdater.apply(t));
    }
}
