package com.example.demo.repositories;

import com.example.demo.Task;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.google.common.collect.ImmutableList.copyOf;

@Repository
public class InMemoryResultRepository implements ResultRepository {
    private final Map<Task, CopyOnWriteArrayList<Double>> rates = new ConcurrentHashMap<>();

    @Override
    public void addToRate(Task task, Double voteAverage) {
        rates.computeIfAbsent(task, _it -> new CopyOnWriteArrayList<>());

        rates.computeIfPresent(task, (_it, list) -> {
            list.add(voteAverage);
            return list;
        });
    }

    @Override
    public List<Double> getRates(Task task) {
        List<Double> result = rates.get(task);
        if (result == null) {
            return Collections.emptyList();
        } else {
            return copyOf(result);
        }
    }
}
