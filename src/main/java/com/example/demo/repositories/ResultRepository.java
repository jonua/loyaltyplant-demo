package com.example.demo.repositories;

import com.example.demo.Task;

import java.util.List;

public interface ResultRepository {
    void addToRate(Task task, Double voteAverage);

    List<Double> getRates(Task task);
}
