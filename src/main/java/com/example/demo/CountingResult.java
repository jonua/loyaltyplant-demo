package com.example.demo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString(exclude = "passed")
@RequiredArgsConstructor
public final class CountingResult {
    private final Task task;
    private final double avg;
    private final long passed; // progress
}
