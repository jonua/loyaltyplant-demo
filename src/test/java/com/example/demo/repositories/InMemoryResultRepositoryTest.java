package com.example.demo.repositories;

import com.example.demo.Task;
import com.example.demo.TaskStatus;
import com.example.demo.models.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryResultRepositoryTest {
    ResultRepository repo;

    @BeforeEach
    void beforeEach() {
        repo = new InMemoryResultRepository();
    }

    @Test
    void addToRate() {
        Task task = createNewTaskInstance();
        repo.addToRate(task, 1.3);
        repo.addToRate(task,3.14);

        List<Double> rates = repo.getRates(task);

        assertNotNull(rates);
        assertEquals(2, rates.size());
        assertTrue(rates.contains(1.3));
        assertTrue(rates.contains(3.14));
    }

    private Task createNewTaskInstance() {
        return new Task(UUID.randomUUID(), new Genre(1, ""), TaskStatus.NEW, "");
    }
}