package com.example.demo.repositories;

import com.example.demo.Task;
import com.example.demo.TaskStatus;
import com.example.demo.models.Genre;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InMemoryTaskRepositoryTest {

    TaskRepository repo;

    @BeforeEach
    void beforeEach() {
        repo = new InMemoryTaskRepository();
    }

    @Test
    void createTask() {
        Task task = createNewTask();
        Task found = repo.find(task.getId()).get();

        assertEquals(task, found);
        assertEquals(task.hashCode(), found.hashCode());
        assertEquals(task.getGenre(), found.getGenre());
        assertEquals(task.getStatus(), found.getStatus());
    }

    @Test
    void changeStatusToProcessing() {
        Task task = createNewTask();
        repo.changeStatusToProcessing(task.getId());
        Task found = repo.find(task.getId()).get();

        assertEquals(task, found);
        assertEquals(TaskStatus.PROCESSING, found.getStatus());
    }

    @Test
    void changeStatusToStop() {
        Task task = createNewTask();
        repo.changeStatusToStop(task.getId());
        Task found = repo.find(task.getId()).get();

        assertEquals(task, found);
        assertEquals(TaskStatus.CANCELED, found.getStatus());
    }

    @Test
    void changeStatusToError() {
        Task task = createNewTask();

        repo.changeStatusToProcessing(task.getId());

        String errorMessage = "test message";
        repo.changeStatusToError(task.getId(), errorMessage);

        Task found = repo.find(task.getId()).get();
        assertEquals(task, found);
        assertEquals(TaskStatus.ERROR, found.getStatus());
        assertEquals(errorMessage, found.getNote());
    }

    @Test
    void changeStatusToDone() {
        Task task = createNewTask();
        repo.changeStatusToProcessing(task.getId());
        repo.changeStatusToProcessing(task.getId());
        repo.chaneStatusToDone(task.getId());

        Task found = repo.find(task.getId()).get();

        assertEquals(task, found);
        assertEquals(TaskStatus.DONE, found.getStatus());
    }

    @Test
    void unableTo_changeStatusToDone_fromStatusERROR() {
        Task task = createNewTask();
        repo.changeStatusToProcessing(task.getId());
        repo.changeStatusToError(task.getId(), "cause");
        repo.chaneStatusToDone(task.getId());

        Task found = repo.find(task.getId()).get();

        assertEquals(task, found);
        assertEquals(TaskStatus.ERROR, found.getStatus());
    }

    Task createNewTask() {
        Task task = new Task(UUID.randomUUID(), new Genre(1L, ""), TaskStatus.NEW, "");
        return repo.create(task);
    }
}