package com.example.demo;

import com.example.demo.models.Genre;
import com.example.demo.properties.MovieServiceProperties;
import com.example.demo.services.CounterService;
import com.example.demo.services.ResultService;
import com.example.demo.services.TaskService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.SocketUtils;

import java.util.concurrent.TimeUnit;

import static com.example.demo.TestUtils.loadResourceAsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = DemoApplicationAutoConfiguration.class)
class PresentationTest {
    static final long CORRECT_GENRE_ID = 18;
    static final int CORRECT_GENRE_MOVIES_COUNT = 13;
    static final int CORRECT_GENRE_MOVIES_AVERAGE = 7;

    static final long INCORRECT_GENRE_ID = 666;

    static final String GENRES_PATH = "/3/genre/movie/list";
    static final String MOVIES_PATH = "/3/discover/movie";

    static ClientAndServer clientAndServer;

    @Autowired
    MovieServiceProperties props;

    @Autowired
    CounterService counterService;

    @Autowired
    ResultService resultService;

    @Autowired
    private TaskService taskService;

    @BeforeAll
    static void beforeAll() {
        int port = SocketUtils.findAvailableTcpPort();
        System.setProperty("third-party.movie.host", "http://localhost:" + port);
        clientAndServer = new ClientAndServer(port);
    }

    @AfterAll
    static void afterClass() {
        clientAndServer.stop();
    }

    @Test
    void progressAvailableTest() {
        configureMockServer();

        Task createdTask = counterService.startCounting(new Genre(CORRECT_GENRE_ID, ""));

        CountingResult resultsAfter1SecWaiting = resultService.extractForTask(createdTask);
        doSleepingForSeconds(2);
        CountingResult resultsAfter2SecWaiting = resultService.extractForTask(createdTask);

        assertTrue(resultsAfter2SecWaiting.getPassed() > resultsAfter1SecWaiting.getPassed());
    }

    @Test
    void cancelTaskTest() {
        configureMockServer();

        Task createdTask = counterService.startCounting(new Genre(CORRECT_GENRE_ID, ""));
        doSleepingForSeconds(1);
        taskService.cancel(createdTask);
        doSleepingForSeconds(3);

        Task cancelled = taskService.find(createdTask.getId()).get();
        long passed = resultService.extractForTask(createdTask).getPassed();

        assertEquals(TaskStatus.CANCELED, cancelled.getStatus());
        Assertions.assertTrue(passed < CORRECT_GENRE_MOVIES_COUNT);
    }

    @Test
    void countingCompleteTest() {
        configureMockServer();

        Task newTask = counterService.startCounting(new Genre(CORRECT_GENRE_ID, ""));

        doSleepingForSeconds(3);
        CountingResult result = resultService.extractForTask(newTask);

        assertEquals(TaskStatus.DONE, result.getTask().getStatus());
        assertEquals(CORRECT_GENRE_MOVIES_COUNT, result.getPassed());
        assertEquals(CORRECT_GENRE_MOVIES_AVERAGE, result.getAvg(), 1);
    }

    @Test
    void invalidGenrePassedTest() {
        configureMockServer();

        Task newTask = counterService.startCounting(new Genre(INCORRECT_GENRE_ID, ""));

        doSleepingForSeconds(2);
        CountingResult result = resultService.extractForTask(newTask);

        assertEquals(TaskStatus.ERROR, result.getTask().getStatus());
        assertEquals(0, result.getPassed());
        assertEquals(0, result.getAvg(), 1);
    }

    private void configureMockServer() {
        // configure genres response
        clientAndServer.when(
                HttpRequest.request()
                        .withPath(GENRES_PATH)
                        .withQueryStringParameter("api_key", props.getApiKey())
        ).respond(HttpResponse.response()
                .withStatusCode(HttpStatusCode.OK_200.code())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .withBody(loadResourceAsString("genres.json"))
        );

        // configure movies response (page 1) OK 200
        clientAndServer.when(
                HttpRequest.request(MOVIES_PATH)
                        .withQueryStringParameter("api_key", props.getApiKey())
                        .withQueryStringParameter("page", "1")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(200)
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(loadResourceAsString("movies_page1.json"))
                        .withDelay(TimeUnit.SECONDS, 1)
        );

        // configure movies response (page 2) OK 200
        clientAndServer.when(
                HttpRequest.request(MOVIES_PATH)
                        .withQueryStringParameter("api_key", props.getApiKey())
                        .withQueryStringParameter("page", "2")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(200)
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(loadResourceAsString("movies_page2.json"))
                        .withDelay(TimeUnit.SECONDS, 1)
        );

        // configure movies response (page 3) error 500
        clientAndServer.when(
                HttpRequest.request(MOVIES_PATH)
                        .withQueryStringParameter("api_key", props.getApiKey())
                        .withQueryStringParameter("page", "3")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(500)
        );
    }

    @SneakyThrows
    private void doSleepingForSeconds(int seconds) {
        Thread.sleep(1000 * seconds);
    }
}
