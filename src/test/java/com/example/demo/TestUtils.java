package com.example.demo;

import java.io.InputStream;
import java.util.Scanner;

public class TestUtils {
    public static String loadResourceAsString(String resource) {
        InputStream is = loadResource(resource);

        Scanner scanner = new Scanner(is);
        scanner.useDelimiter("\\A");

        return scanner.next();
    }

    private static InputStream loadResource(String resource) {
        return TestUtils.class.getClassLoader().getResourceAsStream(resource);
    }
}
