package com.example.demo.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ArithmeticalMeanAverageCounterTest {
    @Test
    void calculate() {
        AverageCounter counter = new ArithmeticalMeanAverageCounter();
        double result = counter.calculate(Arrays.asList(1.2, 2.4, 4.5, 4D, 6.4));
        Assertions.assertEquals(3.7, result, 1);
    }
}