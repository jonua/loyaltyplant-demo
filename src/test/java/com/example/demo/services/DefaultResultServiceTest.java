package com.example.demo.services;

import com.example.demo.CountingResult;
import com.example.demo.Task;
import com.example.demo.TaskStatus;
import com.example.demo.models.Genre;
import com.example.demo.repositories.InMemoryResultRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

class DefaultResultServiceTest {
    ResultService resultService;
    TaskService taskService;
    AverageCounter averageCounter = new ArithmeticalMeanAverageCounter();

    @BeforeEach
    void beforeEach() {
        taskService = Mockito.mock(TaskService.class);
        resultService = new DefaultResultService(new InMemoryResultRepository(), taskService, averageCounter);
    }

    @Test
    void extractForTask() {
        Task task = createNewTaskInstance();

        doReturn(Optional.of(task)).when(taskService).find(task.getId());

        resultService.addToRate(task, 1.2);
        resultService.addToRate(task, 2.4);
        resultService.addToRate(task, 4.5);
        resultService.addToRate(task, 4D);
        resultService.addToRate(task, 6.4);

        CountingResult result = resultService.extractForTask(task);

        assertEquals(5, result.getPassed());
        assertEquals(3.7, result.getAvg(), 1);
        assertEquals(task, result.getTask());
    }

    private Task createNewTaskInstance() {
        return new Task(UUID.randomUUID(), new Genre(1, ""), TaskStatus.NEW, "");
    }
}